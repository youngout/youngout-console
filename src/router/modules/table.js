/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const tableRouter = {
  path: '/table',
  component: Layout,
  redirect: '/table/complex-table',
  name: 'Table',
  meta: {
    title: 'Table',
    icon: 'table'
  },
  children: [
    {
      path: 'questions',
      component: () => import('@/views/table/questions'),
      name: 'Questions',
      meta: { title: '問題列表' }
    },
    {
      path: 'questions/:id(\\d+)',
      component: () => import('@/views/table/question'),
      name: 'Question',
      meta: { title: '問題細節', noCache: true, activeMenu: '/questions' },
      hidden: true
    },
    {
      path: 'answers',
      component: () => import('@/views/table/answers'),
      name: 'Answers',
      meta: { title: '回答列表' }
    },
    {
      path: 'users',
      component: () => import('@/views/table/users'),
      name: 'Users',
      meta: { title: '使用者列表' }
    },
    {
      path: 'users/:id(\\d+)',
      component: () => import('@/views/table/user'),
      name: 'User',
      meta: { title: '問使用者細節', noCache: true, activeMenu: '/users' },
      hidden: true
    }
    // {
    //   path: 'dynamic-table',
    //   component: () => import('@/views/table/dynamic-table/index'),
    //   name: 'DynamicTable',
    //   meta: { title: 'Dynamic Table' }
    // },
    // {
    //   path: 'drag-table',
    //   component: () => import('@/views/table/drag-table'),
    //   name: 'DragTable',
    //   meta: { title: 'Drag Table' }
    // },
    // {
    //   path: 'inline-edit-table',
    //   component: () => import('@/views/table/inline-edit-table'),
    //   name: 'InlineEditTable',
    //   meta: { title: 'Inline Edit' }
    // },
    // {
    //   path: 'complex-table',
    //   component: () => import('@/views/table/complex-table'),
    //   name: 'ComplexTable',
    //   meta: { title: 'Complex Table' }
    // }
  ]
}
export default tableRouter
